package com.family.tree.models.couples.RESPONSES;

import com.family.tree.enitities.Couple;
import com.family.tree.models.Result;

import java.util.List;

public class GetCouplesResult extends Result
{
    List<Couple> couples;

    public GetCouplesResult(List<Couple> couples)
    {
        this.couples = couples;
    }

    public GetCouplesResult()
    {
    }

    public List<Couple> getCouples()
    {
        return couples;
    }

    public void setCouples(List<Couple> couples)
    {
        this.couples = couples;
    }
}
