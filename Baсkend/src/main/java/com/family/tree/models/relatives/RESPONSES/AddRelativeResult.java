package com.family.tree.models.relatives.RESPONSES;

import com.family.tree.enitities.Relative;
import com.family.tree.models.Result;

public class AddRelativeResult extends Result
{
    Relative relative;

    public AddRelativeResult(Relative relative)
    {
        this.relative = relative;
    }

    public AddRelativeResult()
    {
    }

    public Relative getRelative()
    {
        return relative;
    }

    public void setRelative(Relative relative)
    {
        this.relative = relative;
    }
}
