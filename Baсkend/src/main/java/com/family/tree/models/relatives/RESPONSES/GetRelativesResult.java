package com.family.tree.models.relatives.RESPONSES;

import com.family.tree.enitities.Relative;
import com.family.tree.models.Result;

import java.util.List;

public class GetRelativesResult extends Result
{
    List<Relative> relatives;

    public GetRelativesResult(List<Relative> relatives)
    {
        this.relatives = relatives;
    }

    public GetRelativesResult()
    {
    }

    public List<Relative> getRelatives()
    {
        return relatives;
    }

    public void setRelatives(List<Relative> relatives)
    {
        this.relatives = relatives;
    }
}
