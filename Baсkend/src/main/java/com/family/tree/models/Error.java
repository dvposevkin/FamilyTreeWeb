package com.family.tree.models;

import java.util.List;

public class Error
{
    int code;
    String message;
    List<Validation> validations;

    public Error(int code, String message, List<Validation> validations)
    {
        this.code = code;
        this.message = message;
        this.validations = validations;
    }

    public int getCode()
    {
        return code;
    }

    public void setCode(int code)
    {
        this.code = code;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public List<Validation> getValidations()
    {
        return validations;
    }

    public void setValidations(List<Validation> validations)
    {
        this.validations = validations;
    }
}
