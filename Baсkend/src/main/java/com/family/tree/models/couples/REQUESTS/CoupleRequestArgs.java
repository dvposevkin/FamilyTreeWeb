package com.family.tree.models.couples.REQUESTS;


import com.family.tree.enitities.Couple;
import com.family.tree.models.Args;

public class CoupleRequestArgs extends Args
{
    Couple couple;

    public CoupleRequestArgs(Couple couple)
    {
        this.couple = couple;
    }

    public CoupleRequestArgs()
    {
    }

    public Couple getCouple()
    {
        return couple;
    }

    public void setCouple(Couple couple)
    {
        this.couple = couple;
    }
}
