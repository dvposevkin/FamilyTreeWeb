package com.family.tree.models.couples.RESPONSES;

import com.family.tree.enitities.Line;
import com.family.tree.models.Result;

import java.util.List;

public class GetLinesResult extends Result
{
    List<Line> lines;

    public GetLinesResult(List<Line> lines)
    {
        this.lines = lines;
    }

    public GetLinesResult()
    {
    }

    public List<Line> getLines()
    {
        return lines;
    }

    public void setLines(List<Line> lines)
    {
        this.lines = lines;
    }
}
