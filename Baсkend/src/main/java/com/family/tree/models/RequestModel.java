package com.family.tree.models;

import com.fasterxml.jackson.databind.jsonschema.JsonSerializableSchema;


public class RequestModel<ARGS extends Args>
{
    public String methodName;
    public ARGS args;

    public RequestModel(String methodName, ARGS args)
    {
        this.methodName = methodName;
        this.args = args;
    }

    public RequestModel()
    {
    }

    public String getMethodName()
    {
        return methodName;
    }

    public void setMethodName(String methodName)
    {
        this.methodName = methodName;
    }

    public ARGS getArgs()
    {
        return args;
    }

    public void setArgs(ARGS args)
    {
        this.args = args;
    }
}
