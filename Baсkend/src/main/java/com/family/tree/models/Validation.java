package com.family.tree.models;

public class Validation
{
    int code;
    String parameterName;
    String message;

    public Validation(int code, String parameterName, String message)
    {
        this.code = code;
        this.parameterName = parameterName;
        this.message = message;
    }

    public int getCode()
    {
        return code;
    }

    public void setCode(int code)
    {
        this.code = code;
    }

    public String getParameterName()
    {
        return parameterName;
    }

    public void setParameterName(String parameterName)
    {
        this.parameterName = parameterName;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }
}
