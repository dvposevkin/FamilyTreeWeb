package com.family.tree.models.couples.RESPONSES;

import com.family.tree.enitities.Couple;
import com.family.tree.models.Result;

public class AddCoupleResult extends Result
{
    Couple couple;

    public AddCoupleResult(Couple couple)
    {
        this.couple = couple;
    }

    public AddCoupleResult()
    {
    }

    public Couple getCouple()
    {
        return couple;
    }

    public void setCouple(Couple couple)
    {
        this.couple = couple;
    }
}
