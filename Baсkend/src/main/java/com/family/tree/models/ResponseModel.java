package com.family.tree.models;

public class ResponseModel
{
    Error error;
    Result result;

    public ResponseModel(Error error, Result result)
    {
        this.error = error;
        this.result = result;
    }

    public ResponseModel()
    {
    }

    public Error getError()
    {
        return error;
    }

    public void setError(Error error)
    {
        this.error = error;
    }

    public Result getResult()
    {
        return result;
    }

    public void setResult(Result result)
    {
        this.result = result;
    }
}
