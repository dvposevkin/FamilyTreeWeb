package com.family.tree.models.relatives.REQUESTS;

import com.family.tree.enitities.Relative;
import com.family.tree.models.Args;
import com.fasterxml.jackson.databind.jsonschema.JsonSerializableSchema;


public class RelativeRequestArgs extends Args
{
    public Relative relative;
    Integer pageNumber;

    public RelativeRequestArgs(Relative relative, Integer pageNumber)
    {
        this.relative = relative;
        this.pageNumber = pageNumber;
    }

    public RelativeRequestArgs()
    {
    }

    public Relative getRelative()
    {
        return relative;
    }

    public void setRelative(Relative relative)
    {
        this.relative = relative;
    }

    public Integer getPageNumber()
    {
        return pageNumber;
    }

    public void setPageNumber(Integer pageNumber)
    {
        this.pageNumber = pageNumber;
    }
}
