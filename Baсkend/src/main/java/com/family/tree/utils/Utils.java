package com.family.tree.utils;

public class Utils
{
    public static int getRandom(int lower, int upper)
    {
        int random = (int) (Math.random() * (upper - lower)) + lower;
        return random;
    }
}
