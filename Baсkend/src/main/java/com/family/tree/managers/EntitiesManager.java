package com.family.tree.managers;

import com.family.tree.enitities.Couple;
import com.family.tree.enitities.Line;
import com.family.tree.enitities.Relative;
import com.family.tree.enums.Errors;
import com.family.tree.models.Error;
import com.family.tree.models.ResponseModel;
import com.family.tree.models.Validation;
import com.family.tree.models.couples.RESPONSES.AddCoupleResult;
import com.family.tree.models.couples.RESPONSES.GetCouplesResult;
import com.family.tree.models.couples.RESPONSES.GetLinesResult;
import com.family.tree.models.relatives.RESPONSES.AddRelativeResult;
import com.family.tree.models.relatives.RESPONSES.GetRelativesResult;
import com.family.tree.utils.Utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

public class EntitiesManager extends AbstractManager
{
    // Relatives
    public ResponseModel getAllRelatives()
    {
        ResponseModel model = new ResponseModel();
        model.setError(new Error(Errors.Common.Success, Errors.Common.getErrorMessage(Errors.Common.Success), null));
        List<Relative> allRelatives = new ArrayList<>(relatives.values());
        GetRelativesResult result = new GetRelativesResult(allRelatives);
        model.setResult(result);
        return model;
    }

    public ResponseModel getRelativesByPageNumber(int pageNumber)
    {
        ResponseModel model = new ResponseModel();
        model.setError(new Error(Errors.Common.Success, Errors.Common.getErrorMessage(Errors.Common.Success), null));
        List<Relative> allRelatives = new ArrayList<>();
        for (Iterator iterator = relatives.values().iterator(); iterator.hasNext(); )
        {
            Relative next = (Relative) iterator.next();
            for (Iterator<Integer> relativeIterator = next.getPageNumbers().iterator(); relativeIterator.hasNext(); )
            {
                Integer page = relativeIterator.next();
                if (page.equals(pageNumber))
                {
                    allRelatives.add(next);
                }
            }
        }
        GetRelativesResult result = new GetRelativesResult(allRelatives);
        model.setResult(result);
        return model;
    }

    public ResponseModel addRelative(Relative relative)
    {
        ResponseModel model = new ResponseModel();
        if (relative.getId() == null || relative.getId().equals(""))
        {
            relative.setId(UUID.randomUUID().toString());
        }
        if (relative.getyPosition() < 0) relative.setyPosition(0);
        relatives.put(relative.getId(), relative);
        model.setError(new Error(Errors.Common.Success, Errors.Common.getErrorMessage(Errors.Common.Success), null));
        Relative resultRelative = (Relative) relatives.get(relative.getId());
        AddRelativeResult result = new AddRelativeResult(resultRelative);
        model.setResult(result);
        System.out.println(relative.getSername() + " " + relative.getName() + " " + relative.getMiddleName() + " Added Successfully");
        return model;
    }

    //Couples
    public ResponseModel getAllCouples()
    {
        ResponseModel model = new ResponseModel();
        model.setError(new Error(Errors.Common.Success, Errors.Common.getErrorMessage(Errors.Common.Success), null));
        List<Couple> allCouples = new ArrayList<>(couples.values());
        GetCouplesResult result = new GetCouplesResult(allCouples);
        model.setResult(result);
        return model;
    }

    public ResponseModel addCouple(Couple couple)
    {
        ResponseModel model = new ResponseModel();
        if (couple.getId() == null || couple.getId().equals(""))
        {
            couple.setId(UUID.randomUUID().toString());
        }
        List<Validation> validations = new ArrayList<>();
        if (relatives.containsKey(couple.getFirstCoupleId()) == false)
        {
            Validation validation = new Validation(Errors.Couple.RELATIVE_NOT_FOUND, "firstCoupleId", Errors.Common.getErrorMessage(Errors.Couple.RELATIVE_NOT_FOUND));
            validations.add(validation);
        }
        if (relatives.containsKey(couple.getSecondCoupleId()) == false)
        {
            Validation validation = new Validation(Errors.Couple.RELATIVE_NOT_FOUND, "secondCoupleId", Errors.Common.getErrorMessage(Errors.Couple.RELATIVE_NOT_FOUND));
            validations.add(validation);
        }
        if (validations.size() == 0)
        {
            couples.put(couple.getId(), couple);
            Error error = new Error(Errors.Common.Success, Errors.Common.getErrorMessage(Errors.Common.Success), null);
            model.setError(error);
            AddCoupleResult result = new AddCoupleResult(couple);
            model.setResult(result);
            System.out.println("couple added");
        }
        else if (validations.size() == 1)
        {
            Error error = new Error(validations.get(0).getCode(), validations.get(0).getMessage(), validations);
            model.setError(error);
        }
        return model;
    }

    public ResponseModel getAllLines()
    {
        ResponseModel model = new ResponseModel();
        model.setError(new Error(Errors.Common.Success, Errors.Common.getErrorMessage(Errors.Common.Success), null));
        List<Line> allLines = new ArrayList<>(lines.values());
        GetLinesResult result = new GetLinesResult(allLines);
        model.setResult(result);
        return model;
    }
}
