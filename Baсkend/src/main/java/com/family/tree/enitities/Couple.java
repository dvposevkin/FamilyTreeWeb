package com.family.tree.enitities;

public class Couple
{
    String id;
    String firstCoupleId;
    String secondCoupleId;
    String sername;

    public Couple(String id, String firstCoupleId, String secondCoupleId, String sername)
    {
        this.id = id;
        this.firstCoupleId = firstCoupleId;
        this.secondCoupleId = secondCoupleId;
        this.sername = sername;
    }

    public Couple()
    {
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getFirstCoupleId()
    {
        return firstCoupleId;
    }

    public void setFirstCoupleId(String firstCoupleId)
    {
        this.firstCoupleId = firstCoupleId;
    }

    public String getSecondCoupleId()
    {
        return secondCoupleId;
    }

    public void setSecondCoupleId(String secondCoupleId)
    {
        this.secondCoupleId = secondCoupleId;
    }

    public String getSername()
    {
        return sername;
    }

    public void setSername(String sername)
    {
        this.sername = sername;
    }
}
