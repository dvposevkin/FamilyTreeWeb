package com.family.tree.enitities;

import com.fasterxml.jackson.databind.jsonschema.JsonSerializableSchema;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
public class Relative
{
    String id;
    String birthPlace;
    String education;
    String livingPlace;
    String work;
    Integer familyStatus;
    List<String> familyRelativeIds;
    List<String> childrenIds;
    String lastLivingPlace;
    String phoneNumber;
    String additionalInformation;
    String name;
    String sername;
    String middleName;
    String photoPath;
    String birthDate;
    String deathDate;
    List<Integer> pageNumbers;
    Integer xPosition;
    Integer yPosition;

    public Relative( String birthPlace, String education, String livingPlace, String work, Integer familyStatus, List<String> familyRelativeIds, List<String> childrenIds, String lastLivingPlace, String phoneNumber, String additionalInformation, String name, String sername, String middleName, String photoPath, String birthDate, String deathDate, List<Integer> pageNumbers, Integer xPosition, Integer yPosition)
    {
        this.id = UUID.randomUUID().toString();
        this.birthPlace = birthPlace;
        this.education = education;
        this.livingPlace = livingPlace;
        this.work = work;
        this.familyStatus = familyStatus;
        this.familyRelativeIds = familyRelativeIds;
        this.childrenIds = childrenIds;
        this.lastLivingPlace = lastLivingPlace;
        this.phoneNumber = phoneNumber;
        this.additionalInformation = additionalInformation;
        this.name = name;
        this.sername = sername;
        this.middleName = middleName;
        this.photoPath = photoPath;
        this.birthDate = birthDate;
        this.deathDate = deathDate;
        this.pageNumbers = pageNumbers;
        this.xPosition = xPosition;
        this.yPosition = yPosition;
    }

    public Relative()
    {
        this.id = UUID.randomUUID().toString();
    }

    public void addFamilyRelative(String id)
    {
        if (this.familyRelativeIds == null)
        {
            this.familyRelativeIds = new ArrayList<>();
        }
        this.familyRelativeIds.add(id);
    }

    public void addChildrenId(String id)
    {
        if (this.childrenIds == null)
        {
            this.childrenIds = new ArrayList<>();
        }
        this.childrenIds.add(id);
    }

    public void addPageNumber(Integer pageNumber)
    {
        if (this.pageNumbers == null)
        {
            this.pageNumbers = new ArrayList<>();
        }
        this.pageNumbers.add(pageNumber);
    }


    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getBirthPlace()
    {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace)
    {
        this.birthPlace = birthPlace;
    }

    public String getEducation()
    {
        return education;
    }

    public void setEducation(String education)
    {
        this.education = education;
    }

    public String getLivingPlace()
    {
        return livingPlace;
    }

    public void setLivingPlace(String livingPlace)
    {
        this.livingPlace = livingPlace;
    }

    public String getWork()
    {
        return work;
    }

    public void setWork(String work)
    {
        this.work = work;
    }

    public Integer getFamilyStatus()
    {
        return familyStatus;
    }

    public void setFamilyStatus(Integer familyStatus)
    {
        this.familyStatus = familyStatus;
    }

    public List<String> getFamilyRelativeIds()
    {
        return familyRelativeIds;
    }

    public void setFamilyRelativeIds(List<String> familyRelativeIds)
    {
        this.familyRelativeIds = familyRelativeIds;
    }

    public List<String> getChildrenIds()
    {
        return childrenIds;
    }

    public void setChildrenIds(List<String> childrenIds)
    {
        this.childrenIds = childrenIds;
    }

    public String getLastLivingPlace()
    {
        return lastLivingPlace;
    }

    public void setLastLivingPlace(String lastLivingPlace)
    {
        this.lastLivingPlace = lastLivingPlace;
    }

    public String getPhoneNumber()
    {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    public String getAdditionalInformation()
    {
        return additionalInformation;
    }

    public void setAdditionalInformation(String additionalInformation)
    {
        this.additionalInformation = additionalInformation;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getSername()
    {
        return sername;
    }

    public void setSername(String sername)
    {
        this.sername = sername;
    }

    public String getMiddleName()
    {
        return middleName;
    }

    public void setMiddleName(String middleName)
    {
        this.middleName = middleName;
    }

    public String getPhotoPath()
    {
        return photoPath;
    }

    public void setPhotoPath(String photoPath)
    {
        this.photoPath = photoPath;
    }

    public String getBirthDate()
    {
        return birthDate;
    }

    public void setBirthDate(String birthDate)
    {
        this.birthDate = birthDate;
    }

    public String getDeathDate()
    {
        return deathDate;
    }

    public void setDeathDate(String deathDate)
    {
        this.deathDate = deathDate;
    }

    public List<Integer> getPageNumbers()
    {
        return pageNumbers;
    }

    public void setPageNumbers(List<Integer> pageNumbers)
    {
        this.pageNumbers = pageNumbers;
    }

    public Integer getxPosition()
    {
        return xPosition;
    }

    public void setxPosition(Integer xPosition)
    {
        this.xPosition = xPosition;
    }

    public Integer getyPosition()
    {
        return yPosition;
    }

    public void setyPosition(Integer yPosition)
    {
        this.yPosition = yPosition;
    }
}
