package com.family.tree.enitities;

public class Line
{
    String firstRelativeId;
    String secondRelativeId;
    String lineId;

    public Line(String lineId,String firstRelativeId, String secondRelativeId)
    {
        this.lineId = lineId;
        this.firstRelativeId = firstRelativeId;
        this.secondRelativeId = secondRelativeId;
    }

    public Line()
    {
    }

    public String getLineId()
    {
        return lineId;
    }

    public String getFirstRelativeId()
    {
        return firstRelativeId;
    }

    public void setFirstRelativeId(String firstRelativeId)
    {
        this.firstRelativeId = firstRelativeId;
    }

    public String getSecondRelativeId()
    {
        return secondRelativeId;
    }

    public void setSecondRelativeId(String secondRelativeId)
    {
        this.secondRelativeId = secondRelativeId;
    }
}
