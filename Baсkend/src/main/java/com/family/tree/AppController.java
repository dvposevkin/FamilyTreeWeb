package com.family.tree;

import com.family.tree.enitities.Couple;
import com.family.tree.enitities.Relative;
import com.family.tree.enums.Methods;
import com.family.tree.managers.EntitiesManager;
import com.family.tree.models.RequestModel;
import com.family.tree.models.ResponseModel;
import com.family.tree.models.couples.REQUESTS.CoupleRequestArgs;
import com.family.tree.models.relatives.REQUESTS.RelativeRequestArgs;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class AppController
{
    private final EntitiesManager manager = new EntitiesManager();


    @PostMapping("/relatives")
    public ResponseModel postRelatives(@RequestBody RequestModel<RelativeRequestArgs> model)
    {
        String methodName = model.getMethodName();
        System.out.println(methodName);
        ResponseModel response = null;
        switch (methodName)
        {
            case Methods.Relatives.GET:
            {
                int pageNumber = model.getArgs().getPageNumber();
                if (pageNumber == -1)
                {
                    response = manager.getAllRelatives();
                }
                else
                {
                    response = manager.getRelativesByPageNumber(pageNumber);
                }
            }
            break;

            case Methods.Relatives.ADD:
            {
                Relative relative = model.getArgs().getRelative();
                response = manager.addRelative(relative);
                System.out.println("Relative: " + relative.getSername() + " " + relative.getName() + " " + relative.getMiddleName() + " ADDED;");
            }
            break;
        }
        return response;
    }

    @PostMapping("/couples")
    public ResponseModel postCouples(@RequestBody RequestModel<CoupleRequestArgs> model)
    {
        String methodName = model.getMethodName();
        ResponseModel response = null;
        switch (methodName)
        {
            case Methods.Couple.GET:
            {
                response = manager.getAllCouples();
            }
            break;

            case Methods.Couple.ADD:
            {
                Couple couple = model.args.getCouple();
                response = manager.addCouple(couple);
            }
            break;
        }
        return response;
    }

}
