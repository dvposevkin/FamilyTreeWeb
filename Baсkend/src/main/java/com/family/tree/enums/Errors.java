package com.family.tree.enums;

import java.util.HashMap;

public class Errors
{


    public static class Common
    {
        public static HashMap<Integer, String> errorValues = new HashMap();
        public static final int Success = 0;
        public static final int COMMON_ERROR = 1;
        public static final int MULTIPLE_ERRORS = 2;

        static
        {
            errorValues.put(Success, "Success");
            errorValues.put(COMMON_ERROR, "Common Error 1");
            errorValues.put(MULTIPLE_ERRORS, "Multiple errors");
        }

        public static String getErrorMessage(int code)
        {
            return errorValues.get(code);
        }

    }

    public static class Couple extends Common
    {
        public static final int ADDING_COUPLE_ERROR = 2000;
        public static final int RELATIVE_NOT_FOUND = 2001;

        static
        {
            errorValues.put(ADDING_COUPLE_ERROR,"Adding couple error");
            errorValues.put(RELATIVE_NOT_FOUND, "Relative not found");
        }
    }
}
