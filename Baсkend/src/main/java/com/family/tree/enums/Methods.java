package com.family.tree.enums;

public class Methods
{
    public class Relatives
    {
        public static final String ADD = "Relative.Add";
        public static final String UPDATE = "Relative.Update";
        public static final String GET = "Relative.Get";
        public static final String SEARCH = "Relative.Search";
    }

    public class Couple
    {
        public static final String ADD = "Couple.Add";
        public static final String GET = "Couple.Get";
    }
}
