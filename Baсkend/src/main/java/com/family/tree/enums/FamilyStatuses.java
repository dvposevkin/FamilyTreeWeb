package com.family.tree.enums;

public class FamilyStatuses
{
    public static final Integer SINGLE = 0;
    public static final Integer MARRIED_MAN = 1;
    public static final Integer MARRIED_FEMALE = 2;
    public static final Integer DIVORCED = 3;
}
