import { TestBed } from '@angular/core/testing';

import { RelativesServiceService } from './relatives-service.service';

describe('RelativesServiceService', () => {
  let service: RelativesServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RelativesServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
