import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Relative} from '../entities/Relative';
import {AbstractResponseModel} from '../entities/responseModels/AbstractResponseModel';
import {AbstractRequestModel} from '../entities/requestModels/AbstractRequestModel';
import {GetRelativesArgs} from '../entities/requestModels/relatives/get-relatives-args';
import {Couple} from '../entities/Couple';
import {CoupleRequestArgs} from '../entities/requestModels/couples/couple-request-args';
import {RelativeRequestArgs} from "../entities/requestModels/relatives/relative-request-args";
import {applySourceSpanToExpressionIfNeeded} from "@angular/compiler/src/output/output_ast";

@Injectable({
  providedIn: 'root'
})
export class RelativesServiceService {

  private relativesUrl: string;
  private couplesUrl: string;
  model: Observable<AbstractResponseModel>;
  requestModel: AbstractRequestModel;

  constructor(private http: HttpClient) {
    this.relativesUrl = 'http://localhost:8080/relatives';
    this.couplesUrl = 'http://localhost:8080/couples';
  }

  public getAllRelatives(pageNumber: number): Observable<AbstractResponseModel> {
    this.requestModel = new AbstractRequestModel();
    this.requestModel.methodName = 'Relative.Get';
    const getRelativeArgs: GetRelativesArgs = new GetRelativesArgs();
    getRelativeArgs.pageNumber = pageNumber;
    this.requestModel.args = getRelativeArgs;
    this.model = this.http.post<AbstractResponseModel>(this.relativesUrl, this.requestModel);
    return this.model;
  }

  public createCouple(firstRelative: string, secondRelative: string): Observable<AbstractResponseModel> {
    this.requestModel = new AbstractRequestModel();
    this.requestModel.methodName = 'Couple.Add';
    const coupleRequestArgs: CoupleRequestArgs = new CoupleRequestArgs();
    const couple: Couple = new Couple();
    couple.firstCoupleId = firstRelative;
    couple.secondCoupleId = secondRelative;
    coupleRequestArgs.couple = couple;
    this.requestModel.args = coupleRequestArgs;
    this.model = this.http.post<AbstractResponseModel>(this.couplesUrl, this.requestModel);
    return this.model;
  }

  public createRelative(relative: Relative): Observable<AbstractResponseModel>
  {
    let requestModel: AbstractRequestModel = new AbstractRequestModel();
    requestModel.methodName = 'Relative.Add';
    console.log('creating new relative with method '+this.requestModel.methodName);
    let relativeRequestArgs: RelativeRequestArgs = new RelativeRequestArgs();
    relativeRequestArgs.relative = relative;
    console.log(relativeRequestArgs.relative.sername);
    requestModel.args = relativeRequestArgs;
    console.log('method: '+requestModel.methodName+' args: '+requestModel.args+" x: "+relative.xPosition);
    let model = this.http.post<AbstractResponseModel>(this.relativesUrl, requestModel);
    return model;
  }

  public getAllCouples(): Observable<AbstractResponseModel> {
    this.requestModel = new AbstractRequestModel();
    this.requestModel.methodName = 'Couple.Get';
    this.model = this.http.post<AbstractResponseModel>(this.couplesUrl, this.requestModel);
    return this.model;
  }
}
