

export class Relative {

  id: string;
  name: string;
  sername: string;
  middleName: string;
  birthPlace: string;
  education: string;
  livingPlace: string;
  work: string;
  familyStatus: number;
  familyRelativeIds: string[];
  childrenIds: string[];
  lastLivingPlace: string;
  phoneNumber: string;
  additionalInformation: string;
  photoPath: string;
  birthDate: string;
  deathDate: string;
  pageNumbers: number[];
  xPosition: number;
  yPosition: number;
  style: string;


}
