import {Args} from "../args";
import {Relative} from "../../Relative";

export class RelativeRequestArgs extends Args {
  relative: Relative;
}
