import {Args} from '../args';

export class GetRelativesArgs extends Args {
  pageNumber: number;
}
