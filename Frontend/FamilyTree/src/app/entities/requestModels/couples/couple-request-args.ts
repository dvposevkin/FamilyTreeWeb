import {Args} from '../args';
import {Couple} from '../../Couple';

export class CoupleRequestArgs extends Args
{
  couple: Couple;
}
