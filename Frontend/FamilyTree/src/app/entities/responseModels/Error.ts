import {Validation} from "./Validation";

export class Error {
  public code: number;
  public message: string;
  public validations: Validation[];



}
