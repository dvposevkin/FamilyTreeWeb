import {Result} from "./Result";
import {Error} from "./Error";

export class AbstractResponseModel {
  public error: Error
  public result: Result;
}
