export class Validation {
  code: number;
  message: string;
  parameterName: string;
}
