import {Result} from '../Result';
import {Couple} from '../../Couple';

export class AddCoupleResult extends Result {
  couple: Couple;
}
