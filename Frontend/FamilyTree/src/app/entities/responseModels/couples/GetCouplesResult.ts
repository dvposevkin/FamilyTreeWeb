import {Result} from '../Result';
import {Couple} from '../../Couple';

export class GetCouplesResult extends Result {
  couples: Couple[];
}
