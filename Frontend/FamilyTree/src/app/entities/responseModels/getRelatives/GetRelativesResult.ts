import {Result} from '../Result';
import {Relative} from '../../Relative';

export class GetRelativesResult extends Result {
  relatives: Relative[];
}
