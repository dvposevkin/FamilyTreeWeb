import {Component, Input, OnInit} from '@angular/core';
import {Relative} from '../../entities/Relative';
import {RelativesServiceService} from '../../services/relatives-service.service';
import {AbstractResponseModel} from '../../entities/responseModels/AbstractResponseModel';
import {GetRelativesResult} from '../../entities/responseModels/getRelatives/GetRelativesResult';

@Component({
  selector: 'app-relatives-component',
  templateUrl: './relatives-component.component.html',
  styleUrls: ['./relatives-component.component.css']
})
export class RelativesComponentComponent {


  responseModel: AbstractResponseModel;
  private relativesResult: GetRelativesResult;
  @Input() relative: Relative;

  constructor(private relativesService: RelativesServiceService) {
  }


  ngGetStyle(xPosition: number, yPosition: number): string {
    return 'position: absolute; left: ' + xPosition + 'px; top: ' + yPosition + 'px;';
  }

  public init(relative: Relative) {
    this.relative = relative;
  }
}
