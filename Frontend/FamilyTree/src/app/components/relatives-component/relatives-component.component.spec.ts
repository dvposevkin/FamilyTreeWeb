import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RelativesComponentComponent } from './relatives-component.component';

describe('RelativesComponentComponent', () => {
  let component: RelativesComponentComponent;
  let fixture: ComponentFixture<RelativesComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RelativesComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RelativesComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
