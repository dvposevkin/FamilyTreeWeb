import {Component, Input, OnInit} from '@angular/core';
import {Relative} from '../../entities/Relative';

@Component({
  selector: 'app-relative-card',
  templateUrl: './relative-card.component.html',
  styleUrls: ['./relative-card.component.css']
})
export class RelativeCardComponent implements OnInit {

  @Input() relative: Relative;
  constructor() { }

  ngOnInit(): void {
  }

}
