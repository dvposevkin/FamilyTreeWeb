import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RelativeCardComponent } from './relative-card.component';

describe('RelativeCardComponent', () => {
  let component: RelativeCardComponent;
  let fixture: ComponentFixture<RelativeCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RelativeCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RelativeCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
