import {Component, Input, OnInit} from '@angular/core';
import {Relative} from '../../entities/Relative';

@Component({
  selector: 'app-couple',
  templateUrl: './couple.component.html',
  styleUrls: ['./couple.component.css']
})
export class CoupleComponent implements OnInit {

  @Input() firstRelative: Relative;
  @Input() secondRelative: Relative;
  height: number;
  width: number;

  constructor() {
  }

  ngOnInit(): void {
  }

  getYPosition(): number {
    let yPosition = 0;
    yPosition = this.firstRelative.yPosition - 50;
    this.height = 230;
    // console.log('couple with ' + this.firstRelative.id + ' y: ' + yPosition);
    return yPosition;
  }

  getXposition(): number {
    let xposition = 0;
    // console.log('firstRelX:' + this.firstRelative.xPosition + ' secondRelX: ' + this.secondRelative.xPosition);
    if (this.firstRelative.xPosition < this.secondRelative.xPosition) {
      xposition = this.firstRelative.xPosition - 30;
      this.width = this.secondRelative.xPosition - this.firstRelative.xPosition + 200 + 30 + 30;
    } else if (this.secondRelative.xPosition < this.firstRelative.xPosition) {
      xposition = this.secondRelative.xPosition - 30;
      this.width = this.firstRelative.xPosition - this.secondRelative.xPosition + 30 + 200 + 30;
    }
    // console.log('couple with ' + this.firstRelative.id + ' x: ' + xposition);
    return xposition;
  }


}
