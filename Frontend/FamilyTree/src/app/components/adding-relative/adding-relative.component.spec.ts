import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddingRelativeComponent } from './adding-relative.component';

describe('AddingRelativeComponent', () => {
  let component: AddingRelativeComponent;
  let fixture: ComponentFixture<AddingRelativeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddingRelativeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddingRelativeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
