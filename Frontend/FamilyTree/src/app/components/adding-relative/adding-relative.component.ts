import {Component, Input, OnInit} from '@angular/core';
import {Relative} from '../../entities/Relative';

@Component({
  selector: 'app-adding-relative',
  templateUrl: './adding-relative.component.html',
  styleUrls: ['./adding-relative.component.css']
})
export class AddingRelativeComponent implements OnInit {

  @Input() relative: Relative;
  constructor() { }

  ngOnInit(): void
  {
  }

}
