import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { RelativesComponentComponent } from './components/relatives-component/relatives-component.component';
import {HttpClientModule} from '@angular/common/http';
import { AddingRelativeComponent } from './components/adding-relative/adding-relative.component';
import { RelativeCardComponent } from './components/relative-card/relative-card.component';
import { CoupleComponent } from './components/couple/couple.component';

@NgModule({
  declarations: [
    AppComponent,
    RelativesComponentComponent,
    AddingRelativeComponent,
    RelativeCardComponent,
    CoupleComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
