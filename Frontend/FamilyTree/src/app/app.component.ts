import {Component, OnInit, RendererFactory2} from '@angular/core';
import {Renderer} from '@angular/compiler-cli/ngcc/src/rendering/renderer';
import {Relative} from './entities/Relative';
import {RelativesServiceService} from './services/relatives-service.service';
import {AbstractResponseModel} from './entities/responseModels/AbstractResponseModel';
import {Error} from './entities/responseModels/Error';
import {GetRelativesResult} from './entities/responseModels/getRelatives/GetRelativesResult';
import {animate} from '@angular/animations';
import {Couple} from './entities/Couple';
import {AddCoupleResult} from './entities/responseModels/couples/AddCoupleResult';
import {GetCouplesResult} from './entities/responseModels/couples/GetCouplesResult';
import {RelativesComponentComponent} from "./components/relatives-component/relatives-component.component";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'FamilyTree';
  addingClass = 'userCardVisible';
  addingPosition = '150px';
  class = 'relative';
  relatives: Relative[];
  couples: Couple[];
  responseModel: AbstractResponseModel;
  addingTop = '0px';
  addingLeft = '0px';
  openedRelative: Relative;
  pageNumber = 1;
  private relativesResult: GetRelativesResult;
  private couplesResult: GetCouplesResult;
  creatingCouple = false;
  firstRelativeToCouple = '0';
  secondRelativeToCouple = '0';
  addingLine = false;
  svg = document.getElementById('linesSVG');
  addingRelative = false;
  newRelativeX = 0;
  newRelativeY = 0;
  contentDiv: HTMLElement;
  newDivHeight;
  newDivWidth;
  scrollX: number = 0;
  scrollY: number = 0;

  constructor(private relativesService: RelativesServiceService) {
  }

  ngOnInit(): void {
    this.init();
    window.addEventListener('scroll', (e) => {
      this.scrollX = window.scrollX;
      this.scrollY = window.scrollY;
    })
    this.svg = document.getElementById('linesSVG');
    this.contentDiv = document.getElementById('contentDiv');
  }

  init(): void {
    this.relativesService.getAllRelatives(this.pageNumber).subscribe(data => {
      this.responseModel = data;
      if (this.responseModel.error.code === 0) {
        this.relativesResult = (this.responseModel.result as GetRelativesResult);
        this.relatives = this.relativesResult.relatives;
      }
    });
    this.relativesService.getAllCouples().subscribe(data => {
      this.responseModel = data;
      if (this.responseModel.error.code === 0) {
        this.couplesResult = (this.responseModel.result as GetCouplesResult);
        this.couples = this.couplesResult.couples;
      }
    });
  }

  // tslint:disable-next-line:typedef
  openRelative(relative: Relative) {
    console.log('opening relative');
    this.addingClass = 'userCardVisible';
    const x: number = relative.xPosition + 210;
    const y: number = relative.yPosition + 50;
    this.addingTop = y + 'px';
    this.addingLeft = x + 'px';
    this.openedRelative = relative;

    /*this.svg.addEventListener('click', (e) => {
      this.closeRelativeCard();
    });*/

  }

  // tslint:disable-next-line:typedef
  relativeClick(relative: Relative, event) {
    // console.log(relative.yPosition);
    const relativeId = relative.id;
    // console.log(relativeId);
    if (this.creatingCouple === true) {
      this.makeCouple(relativeId);
    } else if (this.addingLine === true) {
      this.createLine(relativeId);
    } else {
      this.openRelative(relative);
    }
    event.stopPropagation();
    this.init();
  }

  // tslint:disable-next-line:typedef
  makeCouple(relativeId) {
    if (this.firstRelativeToCouple === '0') {
      this.firstRelativeToCouple = relativeId;
    } else if (this.secondRelativeToCouple === '0') {
      this.secondRelativeToCouple = relativeId;
      if (this.firstRelativeToCouple === this.secondRelativeToCouple) {
        alert('Не надо объединять родственника с самим собой!!!');
        this.firstRelativeToCouple = '0';
        this.firstRelativeToCouple = '0';
      } else {
        this.relativesService.createCouple(this.firstRelativeToCouple, this.secondRelativeToCouple).subscribe(data => {
          this.responseModel = data;
          if (this.responseModel.error.code === 0) {
            if (this.responseModel.result instanceof AddCoupleResult) {
              const addCoupleResult: AddCoupleResult = this.responseModel.result;
              const couple: Couple = addCoupleResult.couple;
              this.init();
            }
            this.firstRelativeToCouple = '0';
            this.firstRelativeToCouple = '0';
            this.creatingCouple = false;
            // tslint:disable-next-line:label-position

          } else {
            this.firstRelativeToCouple = '0';
            this.firstRelativeToCouple = '0';
            this.creatingCouple = false;
          }
        });
      }
    }
  }


  // tslint:disable-next-line:typedef
  openPage(pageNumber: number) {
    this.pageNumber = pageNumber;
    this.init();
  }

  // tslint:disable-next-line:typedef
  createLine(relativeId) {
    if (this.firstRelativeToCouple === '0') {
      this.firstRelativeToCouple = relativeId;

    } else if (this.secondRelativeToCouple === '0') {
      this.secondRelativeToCouple = relativeId;

      if (this.firstRelativeToCouple === this.secondRelativeToCouple) {
        alert('Не надо объединять родственника с самим собой!!!');
        this.firstRelativeToCouple = '0';
        this.firstRelativeToCouple = '0';
      } else {
        this.drawLine();
        this.firstRelativeToCouple = '0';
        this.firstRelativeToCouple = '0';
        this.creatingCouple = false;
        this.addingLine = false;
      }
    }
  }

  // tslint:disable-next-line:typedef
  getFirstRelative(couple: Couple) {
    const relative: Relative = this.relatives.find(x => x.id === couple.firstCoupleId);
    return relative;
  }

  // tslint:disable-next-line:typedef
  getSecondRelative(couple: Couple) {
    const relative: Relative = this.relatives.find(x => x.id === couple.secondCoupleId);
    return relative;
  }

  // tslint:disable-next-line:typedef
  startCreateCouple() {
    if (this.creatingCouple === false) {
      this.creatingCouple = true;
      this.firstRelativeToCouple = '0';
      this.secondRelativeToCouple = '0';
    } else {
      this.creatingCouple = false;
      this.firstRelativeToCouple = '0';
      this.secondRelativeToCouple = '0';
    }
  }

  // tslint:disable-next-line:typedef
  drawLine() {
    const line = document.createElementNS('http://www.w3.org/2000/svg', 'line');
    const relative1: Relative = this.relatives.find(relative => relative.id === this.firstRelativeToCouple);
    const relative2: Relative = this.relatives.find(relative => relative.id === this.secondRelativeToCouple);
    line.setAttribute('stroke-width', '1px');
    line.setAttribute('stroke', '#FFFFFF');
    // line.setAttribute('marker-end', 'url(#triangle)');
    const x1 = relative1.xPosition + 100;
    const x2 = relative2.xPosition + 100;
    const y1 = relative1.yPosition + 130;
    const y2 = relative2.yPosition;
    line.setAttribute('x1', String(x1));
    line.setAttribute('x2', String(x2));
    line.setAttribute('y1', String(y1));
    line.setAttribute('y2', String(y2));
    this.svg.appendChild(line);
  }

  // tslint:disable-next-line:typedef
  closeRelativeCard() {
    if (this.openedRelative !== null) {
      /*console.log('closeCard');
      this.svg.removeEventListener('click', ev => {
        console.log('removing');
        this.closeRelativeCard();
      });*/
      this.addingClass = 'userCardHidden';
      this.openedRelative = null;
    }
  }

  // tslint:disable-next-line:typedef
  startAddingLine() {
    if (this.addingLine === false) {
      this.addingLine = true;
      this.firstRelativeToCouple = '0';
      this.secondRelativeToCouple = '0';
    } else {
      this.addingLine = false;
      this.firstRelativeToCouple = '0';
      this.secondRelativeToCouple = '0';
    }
  }

  startAddingRelative() {
    console.log('startAdding');
    this.addingRelative = true;
    this.newDivHeight = this.contentDiv.scrollHeight;
    this.newDivWidth = this.contentDiv.scrollWidth;
    console.log('adding event added');
  }

  relativeAction($event) {
    let targetName = $event.target.value;
    console.log(targetName);
    if (targetName === 'Добавить родственника') {
      this.startAddingRelative();
    }
    else if(targetName==='deleteRelativeAction'){
      console.log('startDeleteRelative');
    }
  }


  moveNewRelative(e: MouseEvent) {
    if (this.addingRelative === true) {
      let x = e.pageX;
      let y = e.pageY;
      this.newRelativeX = x - 100;
      this.newRelativeY = y - 100;
      let contentX = this.newDivWidth - 300;
      let contentY = this.newDivHeight - 300;
      if (x >= contentX) {
        this.newDivWidth = contentX + 1200;
      }
      if (y >= contentY) {
        this.newDivHeight = contentY + 600;
      }
    }
  }

  mainDivClick() {
    if (this.addingRelative) {
      let relative = new Relative();
      relative.xPosition = this.newRelativeX;
      relative.yPosition = this.newRelativeY /*- document.getElementById('Buttons').offsetHeight*/;
      relative.name = 'Денис';
      relative.middleName = 'Валерьевич';
      relative.sername = 'Посевкин';
      relative.pageNumbers = [this.pageNumber];
      this.relativesService.createRelative(relative).subscribe(data => {
        this.responseModel = data;
        if (this.responseModel.error.code === 0) {
          this.init();
        }
      });

      this.addingRelative = false;
      document.getElementById('noRelativeAction').setAttribute('selected','true');
      /* document.getElementById('mainDiv').removeEventListener('click', e => {
         this.mainDivClick();
       });*/
    }
  }

  changeRelativeAction($event: Event) {

  }
}
